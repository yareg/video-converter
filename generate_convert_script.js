'use strict';

const fs = require('fs');
const path = require('path');

const args = process.argv.slice(2);


class TaskGenerator {
  /**
 * @param {String} torrentsDir - directory with downloaded torrents 
 * @param {String} convertedDir - directory for converted files
 */
  constructor(torrentsDir, convertedDir) {
    this.torrentsDir = torrentsDir;
    this.convertedDir = convertedDir;

    this.scriptsDir = __dirname;
    this.videoExtensions = ['mkv', 'ts']; 
    this.tasks = [];

    if (!this.validateInputParams()) {
      this.exit('Wrong parameters passed into script!');
    } 
  }

  validateInputParams() {
    let valid = true;
    if (!fs.existsSync(this.torrentsDir)) {
      console.log(`Wrong dir passsed: ${this.torrentsDir}`);
      valid = false;
    }
    if (!fs.existsSync(this.convertedDir)) {
      console.log(`Wrong dir passsed: ${this.convertedDir}`);
      valid = false;
    }
  
    return valid;
  }

  exit(msg) {
    if (msg) {
      console.log(msg);
    }
    process.exit();
  }

  generate() {
    this.iterateDataDir(this.torrentsDir);
    this.saveTasks();
  }

  iterateDataDir(dataDir) {
    const itemsList = fs.readdirSync(dataDir);
  
    for (const item of itemsList) {
      const filePath = path.join(dataDir, item);
      if (fs.lstatSync(filePath).isDirectory()) {
        this.processDirectory(filePath);
      } else {
        const fileExt = item.split('.').pop();
        if (this.videoExtensions.includes(fileExt)) {
          this.addTask(dataDir, item, dataDir !== this.torrentsDir);
        }
      }
    }
  }

  processDirectory(dirPath) {
    this.iterateDataDir(dirPath);
  }

  addTask(dirPath, fileName, separateFileFromDir) {
    const task = this.generateTask(dirPath, fileName, separateFileFromDir);
    this.tasks.push(task);
  }

  generateTask(dirPath, fileName, separateFileFromDir) {
    const defaultSettings = 'CONFIG_VIDEO_STREAM="0" CONFIG_AUDIO_STREAM="1"';
    const convertCall = `${path.join(this.scriptsDir, 'torrent-scripts.sh')} "${dirPath}" "${fileName}" "${this.convertedDir}" ${separateFileFromDir.toString()}`;
    const convertCommand = `${defaultSettings} ${convertCall}`;

    return convertCommand;
  }

  saveTasks() {
    const tasksFilePath = path.join(this.scriptsDir, 'tasks.txt');
    if (fs.existsSync(tasksFilePath)) {
      fs.unlinkSync(tasksFilePath);
    }

    const writeStream = fs.createWriteStream(tasksFilePath);
    const pathName = writeStream.path;
    this.tasks.forEach(value => writeStream.write(`${value}\n`));
    writeStream.on('finish', () => {
      console.log(`Tasks successfully saved into file ${pathName}`);
    });

    writeStream.on('error', (err) => {
      console.error(`Error saving tasks file: ${err}`)
    });
  }
}

const taskGenerator = new TaskGenerator(args[0], args[1]);
taskGenerator.generate();

