#!/bin/bash

function apply_config {
  local CONFIG_FILE="$1"
  if [ ! -f "$CONFIG_FILE" ] || [ ! -r "$CONFIG_FILE" ];then
    echo "Config file must be provided!"
    exit 1
  fi

  local TMP_CONFIG_FILE="$(mktemp)"
  local _IFS="$IFS"
  IFS=$'\n'
  for LINE in `cat $CONFIG_FILE`; do
    local OPTION_KEY="$(_get_option_key $LINE)"
    local REGEX="^$OPTION_KEY=.*"
      # if config option is not set in env variables
      if (( $(printenv | egrep "$REGEX" | wc -l ) == 0 ));then
        echo "$LINE" >> "$TMP_CONFIG_FILE"
      fi 
  done
  source "$TMP_CONFIG_FILE"
  rm -f "$TMP_CONFIG_FILE"
  IFS="$_IFS"
}

function _get_option_key {
  local OPTION="$1"
  echo "$OPTION" | awk -F "=" '{print $1}' | sed 's#"##g'
}

# $1 - file name
function file_ext {
  local FILE_NAME="$1"
  if [ -z "$FILE_NAME" ] || [ ! -f "$FILE_NAME" ];then
    log "Wrong file passed: $FILE_NAME"
    exit 1
  fi  

  echo "${FILE_NAME##*.}"
}

function get_timestamp {
  date +%s    
}

# $1 - start duration time
function duration_tooks {
  if [ -z "$1" ];then
    log "Start time must be passed!"
    exit 1
  fi

  local START_TIME="$1"
  local END_TIME=$(get_timestamp)

  local DURATION=$(($END_TIME-$START_TIME))

  echo "$(convertsecs $DURATION)"
}

convertsecs() {
  ((h=${1}/3600))
  ((m=(${1}%3600)/60))
  ((s=${1}%60))
  printf "%02d:%02d:%02d\n" $h $m $s
}

# $1 - string
function trim_quotes {
  UNQUOTED="$1"
  if [ -z "$UNQUOTED" ];then
    log "Param to function trim_quotes must be passed!"
    exit 1
  fi  

  UNQUOTED="${UNQUOTED##\"}"
  UNQUOTED="${UNQUOTED%%\"}"  

  echo "$UNQUOTED"  
}

# $1 - string
function sanitize_special_chars {
  local INPUT="$1"
  local SANITIZED="${INPUT/\'/}"
  SANITIZED="$(echo $SANITIZED | sed 's#\[##g' | sed 's#\]##g' | sed 's#(##g' | sed 's#)##g')"

  echo "$SANITIZED"
}

function generate_id {
  LC_ALL=C tr -dc 'A-Za-z0-9' < /dev/urandom | fold -w 8 | head -1
}