#!/bin/bash

# $1 - directory where files are located, %D parameter in uTorrent
# $2 - file name, requred for single files, ignoring for converting directories, %F parameter in uTorrent
# $3 - base directory to locate converted files
# $4 - optional - bool - has to be true if is convering single file from directory
function main {
  local SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  
  source "$SCRIPT_DIR/functions.sh"
  apply_config "$SCRIPT_DIR/config.conf"
  source "$SCRIPT_DIR/log.sh"
  source "$SCRIPT_DIR/process_video.sh"
  source "$SCRIPT_DIR/ts2mkv.sh"

  local SAVE_DIR="$1"
  # remove quotes, if uTorrent passed quoted path 
  SAVE_DIR=$(trim_quotes "$SAVE_DIR")
  local FILE_NAME="$2"
  FILE_NAME=$(trim_quotes "$FILE_NAME")

  local BASE_CONVERTED_DIR="$3"
  local DOWNLOAD_SINGLE_FILE="$4"
  if [ -z "$DOWNLOAD_SINGLE_FILE" ];then
    DOWNLOAD_SINGLE_FILE=false
  fi

  CONVERT_LOG_FILE="$SCRIPT_DIR/$CONFIG_LOG_FILE"

  log "NEW converting process started: $SCRIPT_DIR/torrent-scripts.sh \"$SAVE_DIR\" \"$FILE_NAME\" \"$BASE_CONVERTED_DIR\" $DOWNLOAD_SINGLE_FILE"
  log "with options: \$CONFIG_TORRENTS_DIR: $CONFIG_TORRENTS_DIR, \$CONFIG_LOG_FILE: $CONFIG_LOG_FILE, \$CONFIG_VIDEO_STREAM: $CONFIG_VIDEO_STREAM, \$CONFIG_AUDIO_STREAM: $CONFIG_AUDIO_STREAM, \$CONFIG_SCALE: $CONFIG_SCALE"

  cd "$SAVE_DIR"

  if [[ "$SAVE_DIR" == *"$CONFIG_TORRENTS_DIR" ]] || [ "$DOWNLOAD_SINGLE_FILE" = true ];then
    if [ "$DOWNLOAD_SINGLE_FILE" = true ] &&  [[ "$SAVE_DIR" != *"$CONFIG_TORRENTS_DIR" ]];then
      local CREATE_DIR=true
    else 
      local CREATE_DIR=false
    fi

    log "Convert file: $FILE_NAME"
    log "Current directory: $(pwd)"
    log "+ process_video \"$BASE_CONVERTED_DIR\" $CREATE_DIR \"\" \"$FILE_NAME\""
    process_video "$BASE_CONVERTED_DIR" $CREATE_DIR "" "$FILE_NAME"
  else
    log "Convert directory: $SAVE_DIR"
    log "Current directory: $(pwd)"
    log "+ process_video \"$BASE_CONVERTED_DIR\" true \"(\.mkv|\.ts)\$\""
    local DIR_CONV_STARTED="$(get_timestamp)"
    process_video "$BASE_CONVERTED_DIR" true "(\.mkv|\.ts)$"
    log "Directory $SAVE_DIR successfully converted in $(duration_tooks $DIR_CONV_STARTED)"
  fi
}

main "$1" "$2" "$3" "$4"
