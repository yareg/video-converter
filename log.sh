#!/bin/bash

# $1 - log message
# $2 - log in both targets: file and console (default: true)
function log {
  if [ -z "$CONVERT_LOG_FILE" ];then
    echo "Log file must be set!"
    exit 1
  fi  
  local MSG="$1"
  local LOG_BOTH_TARGETS="$2"
  if [ -z "$LOG_BOTH_TARGETS" ];then
    LOG_BOTH_TARGETS=true
  fi
  local LOG_FILE="$CONVERT_LOG_FILE"

  if [ -z "$SESSION_ID" ];then
    SESSION_ID="$(generate_id)"
  fi

  echo "[ $(date "+%Y-%m-%d %T") ] [ $SESSION_ID ] $MSG" >> "$LOG_FILE"

  # if OS Windows
  local UNAME=$(uname -s); 
  if [ "${UNAME:0:10}" == "MINGW64_NT" ] || [ "${UNAME:0:10}" == "MINGW32_NT" ];then
    echo -e "\r\n" >> "$LOG_FILE"
  fi  

  if [ "$LOG_BOTH_TARGETS" = true ];then
    print_purple "$MSG"
  fi  
}

# $1 - text to print in purple
function print_purple {
  local COLOR='\033[0;35m' # Purple
  local NC='\033[0m' # No Color
  printf "${COLOR}$1${NC}\n"
}