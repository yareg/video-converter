#!/bin/bash

# $1 - input .ts file
# $2 - optional - directory for converting input file to
function ts2mkv {
  if [ ! -f "$1" ];then
    log "Wrong file passed: $1"
    exit 1
  else 
    local INPUT_FILE="$1"
    local OUTPUT_DIR="$2"

    if [ ! $(file_ext "$INPUT_FILE") == "ts" ];then
      log "Wrong ts2mkv function called: for file $INPUT_FILE"
      exit 1 
    fi

    if [ -z "$OUTPUT_DIR" ];then
      OUTPUT_DIR="."
    fi

    if [ ! -d "$OUTPUT_DIR" ] || [ ! -w "$OUTPUT_DIR" ];then
      log "Wrong output directory passed: $OUTPUT_DIR"
      exit 1
    fi

    local BASE_NAME="${INPUT_FILE%.*}"

    log "Started converting file $INPUT_FILE to .mkv"
    
    TIME_STARTED="$(get_timestamp)"
    log "+ ffmpeg -i \"$INPUT_FILE\"  -map 0:0 -map 0:$CONFIG_AUDIO_STREAM -acodec copy -vcodec copy -sn \"$OUTPUT_DIR/$BASE_NAME\".mp4"
    time ffmpeg -i "$INPUT_FILE"  -map 0:0 -map 0:$CONFIG_AUDIO_STREAM -acodec copy -vcodec copy -sn "$OUTPUT_DIR/$BASE_NAME".mp4 
    log "+ ffmpeg -i \"$OUTPUT_DIR/$BASE_NAME\".mp4 -vcodec copy -acodec copy \"$OUTPUT_DIR/$BASE_NAME\".mkv"
    time ffmpeg -i "$OUTPUT_DIR/$BASE_NAME".mp4 -vcodec copy -acodec copy "$OUTPUT_DIR/$BASE_NAME".mkv 
    if [ -f "$OUTPUT_DIR/$BASE_NAME".mkv  ];then
      log "+ rm -f \"$OUTPUT_DIR/$BASE_NAME\".mp4"
      rm -f "$OUTPUT_DIR/$BASE_NAME".mp4    

      log "File $INPUT_FILE successfully converted to $OUTPUT_DIR/$BASE_NAME'.mkv' in $(duration_tooks $TIME_STARTED)"
    else
      log "Converting $INPUT_FILE to .mkv got some ERRORS..."  
    fi    
    
    
  fi
}