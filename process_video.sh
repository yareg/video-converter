#!/bin/bash

# in directory for all files:
# change resolutions, delete all unnecessary streams (audio, subtitles)
# $1 - base target directory for converted files
# $2 - create directory in destination dir (default true)
# $3 - regexp to select files to convert (default \.mkv$)
# $4 - file name to be converted, if set - $3 will be skipped
function process_video {
  local BASE_CONVERTED_DIR="$1"
  if [ -z "$BASE_CONVERTED_DIR" ] || [ ! -d "$BASE_CONVERTED_DIR" ] || [ ! -w "$BASE_CONVERTED_DIR" ]; then
    log "Wrong base target dir set: $BASE_CONVERTED_DIR !"
    exit 1
  fi  
  
  if [ -z "$2" ];then
    local CREATE_DIR=true
  else
    local CREATE_DIR="$2"
  fi
  
  if [ -z "$3" ];then
    local REGEX="\.mkv$"
  else
    local REGEX="$3"
  fi
  
  local SOURCE_FILE_NAME="$4"

  local FILES
  local TARGET_FILE
  local TARGET_DIR
  local TIME_STARTED
  local CONVERT_FILE_NAME

  local SAVEIFS=$IFS
  IFS=$(echo -en "\n\b")

  if [ -z "$SOURCE_FILE_NAME" ];then
    FILES=($(/bin/ls | egrep "$REGEX" | egrep -v "^crop-" ))
  else
    FILES=("$SOURCE_FILE_NAME")
  fi

  for CONVERT_FILE_NAME in ${FILES[@]}; do
    local FILE_NAME="$CONVERT_FILE_NAME"
    local CONVERTED_FROM_TS=false
    log "To be converted: $FILE_NAME"
    TIME_STARTED="$(get_timestamp)"
    local CURRENT_DIR=${PWD##*/}
    if [ "$CREATE_DIR" = true ]; then
      TARGET_DIR="$BASE_CONVERTED_DIR/$CURRENT_DIR"
    else
      TARGET_DIR="$BASE_CONVERTED_DIR"
    fi
    TARGET_DIR=$(sanitize_special_chars "$TARGET_DIR")

    if [ $(file_ext "$FILE_NAME") == "ts" ];then
      log "+ ts2mkv \"$FILE_NAME\"" 
      ts2mkv "$FILE_NAME"
      FILE_NAME="${FILE_NAME%.*}.mkv"
      CONVERTED_FROM_TS=true
    fi  

    TARGET_FILE="$TARGET_DIR"/$(sanitize_special_chars "$FILE_NAME")
    local TARGET_TMP_FILE="$TARGET_DIR/"convert-$(sanitize_special_chars "$FILE_NAME")

    log "Output file: $TARGET_FILE"
    CROPPED_NAME="crop-$FILE_NAME"
    mkdir -p "$TARGET_DIR"

    if [ "$CONVERTED_FROM_TS" = true ];then
      CROPPED_NAME="$FILE_NAME"
    else 
      rm -f "$CROPPED_NAME"
      log "+ ffmpeg -i \"$FILE_NAME\" -map 0:$CONFIG_VIDEO_STREAM -map 0:$CONFIG_AUDIO_STREAM -acodec copy -vcodec copy -sn \"$CROPPED_NAME\"" 
      ffmpeg -i "$FILE_NAME" -map 0:$CONFIG_VIDEO_STREAM -map 0:$CONFIG_AUDIO_STREAM -acodec copy -vcodec copy -sn "$CROPPED_NAME" 
    fi

    if [ $(file_ext "$FILE_NAME") == "mkv" ];then 
      rm -f "$TARGET_TMP_FILE"
      log "+ ffmpeg -i \"$CROPPED_NAME\" -vf scale=$CONFIG_SCALE \"$TARGET_TMP_FILE\""
      ffmpeg -i "$CROPPED_NAME" -vf scale=$CONFIG_SCALE -max_muxing_queue_size 1024 "$TARGET_TMP_FILE" 
      if [ -f "$TARGET_TMP_FILE" ];then
        log "+ rm -f \"$CROPPED_NAME\""
        rm -f "$CROPPED_NAME"

        log "+ mv \"$TARGET_TMP_FILE\" \"$TARGET_FILE\""
        mv "$TARGET_TMP_FILE" "$TARGET_FILE"

        log "File $CONVERT_FILE_NAME successfully converted in $(duration_tooks $TIME_STARTED)"
      else
        log "Something went WRONG for file: $CONVERT_FILE_NAME, converting duration: $(duration_tooks $TIME_STARTED)"
      fi
    fi  
  done;

  IFS=$SAVEIFS
}